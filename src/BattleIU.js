// noinspection TsLint
var fs = require("fs");
// noinspection TsLint
var phaser = require("phaser");
// noinspection TsLint
var native = require("./native");
var dialog = require("dialog");
var config = {
    height: window.innerHeight,
    width: window.innerWidth,
    type: Phaser.AUTO,
    parent: "game",
    backgroundColor: "#93e7ff",
    pixelArt: false,
    physics: {
        default: "arcade",
        arcade: {
            gravity: {
                y: 10,
            },
        },
    },
    scene: {
        key: "main",
        preload: preload,
        create: create,
        update: update,
    },
};
var json = native.hello("/Users/noisyedge/Projects/sopra/group25/log.json");
// @ts-ignore
var map = [];
map.push(json.init.fields);
for (var i = 0; i < json.steps.length; i++) {
    map.push(json.steps[i].fields);
}
for (var i = 1; i < map.length - 1; i++) {
    for (var j = 0; j < map[i + 1].length; j++) {
        var inside = false;
        var nexttile = map[i + 1][j];
        for (var k = 0; k < map[i].length; k++) {
            if (nexttile.x === map[i][k].x &&
                nexttile.y === map[i][k].y) {
                inside = true;
            }
        }
        if (!inside) {
            var insideb = false;
            for (var k = i - 1; k >= 0 && !insideb; k--) {
                for (var l = 0; l < map[k].length; l++) {
                    if (nexttile.x === map[k][l].x &&
                        nexttile.y === map[k][l].y) {
                        insideb = true;
                    }
                    if (insideb) {
                        map[i].push(map[k][l]);
                        break;
                    }
                }
            }
        }
    }
}
var game = new Phaser.Game(config);
var mapwidth = json.init.width;
var mapheight = json.init.height;
var tilewidth = 120;
var tileheight = 140;
var tilemap = new Array(json.init.width * json.init.height);
var antmap = new Map();
var foodmap = new Array(tilemap.length);
var foodantmap = new Map();
var currentturn = 0;
var lastcurrentturn = 0;
var startscale;
var leftkey;
var rightkey;
var upkey;
var downkey;
var zoominkey;
var zoomoutkey;
var nextturnkey;
var lastturnkey;
function preload() {
    this.load.image("plaintile", "assets/Images/grass_05.png");
    this.load.image("hometile", "assets/Images/grass_02.png");
    this.load.image("liontile", "assets/Images/sand_15.png");
    this.load.image("rocktile", "assets/Images/grass_14.png");
    this.load.image("Aant", "assets/Images/ant_player_right.png");
    this.load.image("Bant", "assets/Images/ant_normal_right.png");
    this.load.image("food", "assets/Images/red_mushroom.png");
    this.load.image("lion", "assets/Images/antlion.png");
}
function create() {
    this.events.on("resize", resize, this);
    var len = map[0].length;
    for (var p = 0; p < len; p++) {
        var tile = map[0][p];
        var xp = tile.x * tilewidth + (tile.y % 2 * tilewidth / 2);
        var yp = tile.y * tileheight - (tile.y * tileheight / 4);
        var ypm = yp + tilewidth / 2;
        var xpm = xp + tilewidth / 2;
        var tilename = "";
        switch (tile.type) {
            case "#":
                tilename = "rocktile";
                break;
            case "=":
                tilename = "liontile";
                break;
            case ".":
                tilename = "plaintile";
                break;
            default:
                tilename = "hometile";
                break;
        }
        var tilespr = this.add.sprite(xp, yp, tilename).setOrigin(0, 0);
        tilespr.setDepth(0);
        tilemap[tile.x + tile.y * json.init.width] = tilespr;
        if (tile.ant != null) {
            var side = tile.ant.direction.includes("west") ? "left" : "right";
            var spr = this.add.sprite(xp + tilewidth / 2, yp + tileheight / 2, tile.ant.swarm_id + "ant");
            if (tile.ant.direction === "northwest" || tile.ant.direction === "southeast") {
                spr.setAngle(60);
            }
            if (tile.ant.direction === "northeast" || tile.ant.direction === "southwest") {
                spr.setAngle(-60);
            }
            if (side === "left") {
                spr.setFlipX(true);
            }
            spr.setScale(Math.min(tilewidth / 1.3 / spr.width, tileheight / 1.3 / spr.height));
            spr.setDepth(2);
            antmap.set(tile.ant.id, spr);
            if (tile.ant.carries_food) {
                var foodspr = this.add.image(xpm, ypm, "food");
                if (tile.ant.direction === "northwest" || tile.ant.direction === "southeast") {
                    foodspr.setAngle(60);
                }
                if (tile.ant.direction === "northeast" || tile.ant.direction === "southwest") {
                    foodspr.setAngle(-60);
                }
                foodspr.setScale(Math.min(spr.displayWidth / 3 / foodspr.width, spr.displayHeight / 3 / foodspr.height));
                foodspr.setDepth(3);
                foodantmap.set(tile.ant.id, spr);
            }
        }
        if (tile.type === "=") {
            var spr = this.add.sprite(xpm, ypm, "lion");
            spr.setScale(Math.min(tilewidth / spr.width, tileheight / spr.height));
            spr.setDepth(4);
        }
        if (tile.type !== "=" && tile.type !== "#" && tile.type !== ".") {
            var hometext = new Phaser.GameObjects.Text(this, 0, 0, tile.type, {
                fontSize: "100px",
                fill: "#000000",
                fontFamily: "antfont"
            });
            var graphics = new Phaser.GameObjects.Graphics(this, { x: 0, y: 0 });
            graphics.strokeRect(0, 0, tilewidth, tileheight);
            hometext.setPosition(xpm, ypm + 20);
            hometext.setAlign("center");
            hometext.setOrigin(0.5, 0.5);
        }
        if (tile.food != null && tile.food > 0) {
            var spr = this.add.sprite(xpm, ypm, "food").setOrigin(0.5, 0.5);
            spr.setScale(Math.min(tilewidth / 5 / spr.width, tileheight / 5 / spr.height));
            spr.setDepth(1);
            foodmap.push(spr);
        }
    }
    this.input.keyboard.on("keydown_LEFT", function (event) {
        currentturn = Math.max(currentturn - 1, 0);
    });
    this.input.keyboard.on("keydown_RIGHT", function (event) {
        currentturn = Math.min(currentturn + 1, map.length - 1);
    });
    leftkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    rightkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
    upkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    downkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
    zoominkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.E);
    zoomoutkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.Q);
    nextturnkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
    lastturnkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.DOWN);
    this.cameras.main.centerOn(0, 0);
    startscale = Math.min(this.cameras.main.width / (mapwidth * tilewidth + tilewidth / 2), this.cameras.main.height / (mapheight * tileheight - ((mapheight - 1) * tileheight / 4)));
    this.cameras.main.setBounds(0, 0, mapwidth * tilewidth + tilewidth / 2, mapheight * tileheight - ((mapheight - 1) * tileheight / 4)).setZoom(startscale);
}
function update() {
    if (upkey.isDown) {
        this.cameras.main.scrollY = this.cameras.main.scrollY - 50;
    }
    if (downkey.isDown) {
        this.cameras.main.scrollY = this.cameras.main.scrollY + 50;
    }
    if (leftkey.isDown) {
        this.cameras.main.scrollX = this.cameras.main.scrollX - 50;
    }
    if (rightkey.isDown) {
        this.cameras.main.scrollX = this.cameras.main.scrollX + 50;
    }
    if (zoominkey.isDown) {
        this.cameras.main.zoom *= 1.05;
    }
    if (zoomoutkey.isDown) {
        this.cameras.main.zoom = Math.max(this.cameras.main.zoom * 0.95, 0.001);
    }
    if (nextturnkey.isDown) {
        currentturn = Math.min(currentturn + 1, map.length - 1);
    }
    if (lastturnkey.isDown) {
        currentturn = Math.max(currentturn - 1, 0);
    }
    if (currentturn !== lastcurrentturn) {
        lastcurrentturn = currentturn;
        Array.from(antmap.values()).forEach(function (ant) { return ant.setVisible(false); });
        for (var i = 0; i < map[currentturn].length; i++) {
            var tile = map[currentturn][i];
            var xp = tile.x * tilewidth + (tile.y % 2 * tilewidth / 2);
            var yp = tile.y * tileheight - (tile.y * tileheight / 4);
            var ypm = yp + tilewidth / 2;
            var xpm = xp + tilewidth / 2;
            if (tile.food != null && tile.food > 0) {
                if (foodmap[tile.x + tile.y * mapwidth] == null) {
                    var spr = this.add.sprite(xpm, ypm, "food").setOrigin(0.5, 0.5);
                    spr.setScale(Math.min(tilewidth / 5 / spr.width, tileheight / 5 / spr.height));
                    foodmap.push(spr);
                }
            }
            else {
                if (foodmap[tile.x + tile.y * mapwidth] != null) {
                    foodmap[tile.x + tile.y * mapwidth].setVisible(false);
                }
            }
            if (tile.ant != null) {
                var spr = antmap.get(tile.ant.id);
                spr.setPosition(xpm, ypm);
                if (tile.ant.direction === "northwest" || tile.ant.direction === "southeast") {
                    spr.setAngle(60);
                }
                else if (tile.ant.direction === "northeast" || tile.ant.direction === "southwest") {
                    spr.setAngle(-60);
                }
                else {
                    spr.setAngle(0);
                }
                spr.setDepth(2);
                spr.setFlipX(tile.ant.direction.includes("west"));
                spr.setVisible(true);
                if (tile.ant.carries_food) {
                    var foodspr = foodantmap.get(tile.ant.id);
                    if (foodspr == null) {
                        foodspr = this.add.image(xpm, ypm, "food");
                        foodantmap.set(tile.ant.id, foodspr);
                    }
                    foodspr.setPosition(xpm, ypm);
                    if (tile.ant.direction === "northwest" || tile.ant.direction === "southeast") {
                        foodspr.setAngle(60);
                    }
                    else if (tile.ant.direction === "northeast" || tile.ant.direction === "southwest") {
                        foodspr.setAngle(-60);
                    }
                    else {
                        foodspr.setAngle(0);
                    }
                    foodspr.setScale(Math.min(spr.displayWidth / 5 / foodspr.width, spr.displayHeight / 5 / foodspr.height));
                    foodspr.setDepth(3);
                    foodspr.setVisible(true);
                }
                else {
                    var foodspr = foodantmap.get(tile.ant.id);
                    if (foodspr != null) {
                        foodspr.setVisible(false);
                    }
                }
            }
        }
    }
}
function resize(width, height) {
    if (width === undefined) {
        width = this.game.config.width;
    }
    if (height === undefined) {
        height = this.game.config.height;
    }
    this.cameras.resize(width, height);
}
window.addEventListener("resize", function (event) {
    game.resize(window.innerWidth, window.innerHeight);
    //   window.innerHeight / (mapheight * tileheight - ((mapheight - 1) * tileheight / 4)));
}, false);
