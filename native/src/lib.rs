#[macro_use]
extern crate neon;

#[macro_use]
extern crate serde_derive;
extern crate sysinfo;
extern crate serde;
extern crate serde_json;
extern crate libflate;


use neon::prelude::*;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;
use std::vec::Vec;
use std::fs;
use sysinfo::SystemExt;
use neon::result::Throw;
use libflate::gzip::Decoder;

fn hello(mut cx: FunctionContext) -> JsResult<JsObject> {
    let arg: Handle<JsString> = cx.argument(0)?;
    let path = arg.value();
    let gamedata = load_data(&path);
    let gamedata_js = JsObject::new(&mut cx);
    let init_js = JsObject::new(&mut cx);
    let steps_js = JsArray::new(&mut cx, gamedata.steps.len() as u32);
    let context = &mut cx;
    let width_js = JsNumber::new(context, gamedata.init.width);
    let height_js = JsNumber::new(context, gamedata.init.height);
    init_js.set(context, "width", width_js)?;
    init_js.set(context, "height", height_js)?;
    let brains_js = JsArray::new(context, gamedata.init.brains.len() as u32);

    for (i, brain) in gamedata.init.brains.iter().enumerate() {
        let brain_js = JsObject::new(context);
        let brain_name_js = JsString::new(context, &brain.name);
        let brain_swarm_id_js = JsString::new(context, &brain.swarm_id);
        let brain_instructions_js = JsArray::new(context, brain.instructions.len() as u32);
        let mut instruction_js;
        for (j, instruction) in brain.instructions.iter().enumerate() {
            instruction_js = JsString::new(context, instruction);
            brain_instructions_js.set(context, j as u32, instruction_js)?;
        }
        brain_js.set(context, "name", brain_name_js)?;
        brain_js.set(context, "swarm_id", brain_swarm_id_js)?;
        brain_js.set(context, "instructions", brain_instructions_js)?;
        brains_js.set(context, i as u32, brain_js)?;
    }

    init_js.set(context, "brains", brains_js)?;

    fields_to_js(context, &gamedata.init.fields, init_js)?;

    steps_to_js(context, gamedata.steps, steps_js)?;


    gamedata_js.set(context, "init", init_js)?;
    gamedata_js.set(context, "steps", steps_js)?;


    Ok(gamedata_js)
}

fn fields_to_js(context: &mut FunctionContext, fields: &Vec<FieldData>, target: Handle<JsObject>) -> Result<bool, Throw> {
    let fields_js = JsArray::new(context, fields.len() as u32);
    for (i, field) in fields.iter().enumerate() {
        let field_js = JsObject::new(context);
        let x_js = JsNumber::new(context, field.x);
        field_js.set(context, "x", x_js)?;
        let y_js = JsNumber::new(context, field.y);
        field_js.set(context, "y", y_js)?;
        let markers_js = JsArray::new(context, field.markers.len() as u32);
        for (j, marker) in field.markers.iter().enumerate() {
            let marker_js = JsObject::new(context);
            let marker_swarm_id_js = JsString::new(context, marker.swarm_id.clone());
            let marker_values_js = JsArray::new(context, marker.values.len() as u32);
            for (k, val) in marker.values.iter().enumerate() {
                let value = JsBoolean::new(context, *val);
                marker_values_js.set(context, k as u32, value)?;
            }
            marker_js.set(context, "swarm_id", marker_swarm_id_js)?;
            marker_js.set(context, "values", marker_values_js)?;
            markers_js.set(context, j as u32, marker_js)?;
        }
        field_js.set(context, "markers", markers_js)?;
        let fieldtype_js = JsString::new(context, field.field_type.clone());
        field_js.set(context, "type", fieldtype_js)?;
        if let Some(food) = field.food {
            let food_js = JsNumber::new(context, food);
            field_js.set(context, "food", food_js)?;
        }
        if let Some(ant) = field.ant.clone() {
            ant_to_js(context, ant, field_js)?;
        }
        fields_js.set(context, i as u32, field_js)?;
    }
    target.set(context, "fields", fields_js)
}

fn ant_to_js(context: &mut FunctionContext, ant: AntData, target: Handle<JsObject>) -> Result<bool, Throw> {
    let ant_js = JsObject::new(context);
    let id_js = JsNumber::new(context, ant.id);
    let pc_js = JsNumber::new(context, ant.program_counter);
    let swarm_id_js = JsString::new(context, ant.swarm_id);
    let carry_js = JsBoolean::new(context, ant.carries_food);
    let direction_js = JsString::new(context, ant.direction);
    let rest_js = JsNumber::new(context, ant.rest_time);
    let register_js = JsArray::new(context, ant.register.len() as u32);
    for (i, reg) in ant.register.iter().enumerate() {
        let reg_js = JsBoolean::new(context, *reg);
        register_js.set(context, i as u32, reg_js)?;
    }
    ant_js.set(context, "id", id_js)?;
    ant_js.set(context, "program_counter", pc_js)?;
    ant_js.set(context, "swarm_id", swarm_id_js)?;
    ant_js.set(context, "carries_food", carry_js)?;
    ant_js.set(context, "direction", direction_js)?;
    ant_js.set(context, "rest_time", rest_js)?;
    ant_js.set(context, "register", register_js)?;
    target.set(context, "ant", ant_js)
}

fn steps_to_js(context: &mut FunctionContext, steps: Vec<StepData>, target: Handle<JsArray>) -> Result<bool, Throw> {
    for (i, step) in steps.iter().enumerate() {
        let step_js = JsObject::new(context);
        let standings_js = JsArray::new(context, step.standings.len() as u32);
        for (j, standing) in step.standings.iter().enumerate() {
            let standing_js = JsObject::new(context);
            let sid = JsString::new(context, standing.swarm_id.clone());
            let score = JsNumber::new(context, standing.score);
            let antcount = JsNumber::new(context, standing.ants);
            standing_js.set(context, "swarm_id", sid)?;
            standing_js.set(context, "score", score)?;
            standing_js.set(context, "ants", antcount)?;
            standings_js.set(context, j as u32, standing_js)?;
        }
        step_js.set(context, "standings", standings_js)?;
        fields_to_js(context, &step.fields, step_js)?;
        target.set(context, i as u32, step_js)?;
    }

    Ok(true)
}

register_module!(mut cx, {
    cx.export_function("hello", hello)
});


#[derive(Debug, Serialize, Deserialize)]
struct GameData {
    init: InitData,
    steps: Vec<StepData>,
}

#[derive(Debug, Serialize, Deserialize)]
struct InitData {
    width: u8,
    height: u8,
    brains: Vec<BrainData>,
    fields: Vec<FieldData>,
}

#[derive(Debug, Serialize, Deserialize)]
struct BrainData {
    name: String,
    swarm_id: String,
    instructions: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize)]
struct StepData {
    standings: Vec<StandingData>,
    fields: Vec<FieldData>,
}

#[derive(Debug, Serialize, Deserialize)]
struct StandingData {
    swarm_id: String,
    score: u32,
    ants: u32,
}

#[derive(Debug, Serialize, Deserialize)]
struct FieldData {
    x: u8,
    y: u8,
    markers: Vec<MarkerData>,
    #[serde(rename = "type")]
    field_type: String,
    food: Option<u16>,
    ant: Option<AntData>,
}

#[derive(Debug, Serialize, Deserialize)]
struct MarkerData {
    swarm_id: String,
    values: Vec<bool>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct AntData {
    id: u32,
    program_counter: u16,
    swarm_id: String,
    carries_food: bool,
    direction: String,
    rest_time: u8,
    register: Vec<bool>,
}


fn load_data(path: &str) -> GameData {
    let meta = fs::metadata(path).expect(&("can't open file: ".to_owned() + path));
    let filesize = meta.len() as f64;
    let filesize = filesize / 8.0 * 1.6;
    let mut system = sysinfo::System::new();
    system.refresh_all();
    let mut free_memory = system.get_total_memory() - system.get_used_memory();
    free_memory += system.get_total_swap() - system.get_used_swap();
    if free_memory as f64 <= filesize {
        read_file_stream(path)
    } else {
        read_file(path)
    }
}

#[allow(dead_code)]
fn read_file_stream<P: AsRef<Path>>(path: P) -> GameData {
    // Open the file in read-only mode.
    let file = BufReader::new(File::open(path).expect("could not read file"));

    // Read the JSON contents of the file as an instance of `User`.
    serde_json::from_reader(file).unwrap()
}

fn read_file(filepath: &str) -> GameData {
    let mut buffered_reader = BufReader::new(File::open(filepath).expect("could not open file"));
    let mut contents = String::new();
    if filepath.contains(".gz") || filepath.contains(".gzip") {
        let mut decoder = Decoder::new(buffered_reader).expect("is not a gzip file");
        let _number_of_bytes = match decoder.read_to_string(&mut contents) {
            Ok(_number_of_bytes) => _number_of_bytes,
            Err(_err) => 0,
        };
    } else {
        let _number_of_bytes = match buffered_reader.read_to_string(&mut contents) {
            Ok(_number_of_bytes) => _number_of_bytes,
            Err(_err) => 0,
        };
    }
    serde_json::from_str(&contents).unwrap()
}

